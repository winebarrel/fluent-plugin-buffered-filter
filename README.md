# fluent-plugin-buffered-filter

Versatile filtering plugin

[![Gem Version](https://badge.fury.io/rb/fluent-plugin-buffered-filter.png)](http://badge.fury.io/rb/fluent-plugin-buffered-filter)
[![Build Status](https://drone.io/bitbucket.org/winebarrel/fluent-plugin-buffered-filter/status.png)](https://drone.io/bitbucket.org/winebarrel/fluent-plugin-buffered-filter/latest)

## Installation

    $ gem install fluent-plugin-buffered-filter


## Configuration

```
<match my.**>
  type buffered_filter
  filter_path /foo/bar/my_filrer.rb
  #prefix filtered
</match>

<match filtered.my.**>
  type stdout
</match>
```

## Usage

```ruby
# Count the number of keys in the record

proc {|tag, time, record|
  {'key_count' => record.keys}
  # or [{...},{...},{...}, ...]
}
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
