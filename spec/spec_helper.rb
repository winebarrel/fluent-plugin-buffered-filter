require 'fluent/test'
require 'fluent/plugin/out_buffered_filter'
require 'tempfile'
require 'time'

# Disable Test::Unit
module Test::Unit::RunCount; def run(*); end; end

RSpec.configure do |config|
  config.before(:all) do
    Fluent::Test.setup
  end
end

def tempfile(content, basename = nil)
  basename ||= "#{File.basename __FILE__}.#{$$}"

  Tempfile.open(basename) do |f|
    f << content
    f.flush
    f.rewind
    yield(f)
  end
end

def run_driver(options = {})
  options = options.dup

  filter = options[:filter] || (<<-EOS)
proc {|tag, time, record|
  {'record' => [tag, time, record].join(',')}
}
  EOS

  option_keys = [
    :prefix,
  ]

  additional_options = option_keys.map {|key|
    if options[key]
      "#{key} #{options[key]}"
    end
  }.join("\n")

  tag = options[:tag] || 'test.default'

  tempfile(filter, options[:tempfile]) do |f|
    fluentd_conf = <<-EOS
type buffered_filter
filter_path #{f.path}
#{additional_options}
    EOS

    driver = Fluent::Test::OutputTestDriver.new(Fluent::BufferedFilterOutput, tag).configure(fluentd_conf)

    driver.run do
      yield(driver)
    end

    driver.emits
  end
end
