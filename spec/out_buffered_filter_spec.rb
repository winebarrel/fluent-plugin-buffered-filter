describe Fluent::BufferedFilterOutput do
  let(:time) {
    Time.parse('2014-02-08 04:14:15 UTC').to_i
  }

  it 'should be filtered' do
    emits = run_driver do |d|
      (0...10).each do |i|
        d.emit({"key#{i}" => "val#{i}"}, time + i)
      end
    end

    expect(emits).to eq(
      [["filtered.test.default", 1391832855, {"record"=>"test.default,1391832855,{\"key0\"=>\"val0\"}"}],
       ["filtered.test.default", 1391832856, {"record"=>"test.default,1391832856,{\"key1\"=>\"val1\"}"}],
       ["filtered.test.default", 1391832857, {"record"=>"test.default,1391832857,{\"key2\"=>\"val2\"}"}],
       ["filtered.test.default", 1391832858, {"record"=>"test.default,1391832858,{\"key3\"=>\"val3\"}"}],
       ["filtered.test.default", 1391832859, {"record"=>"test.default,1391832859,{\"key4\"=>\"val4\"}"}],
       ["filtered.test.default", 1391832860, {"record"=>"test.default,1391832860,{\"key5\"=>\"val5\"}"}],
       ["filtered.test.default", 1391832861, {"record"=>"test.default,1391832861,{\"key6\"=>\"val6\"}"}],
       ["filtered.test.default", 1391832862, {"record"=>"test.default,1391832862,{\"key7\"=>\"val7\"}"}],
       ["filtered.test.default", 1391832863, {"record"=>"test.default,1391832863,{\"key8\"=>\"val8\"}"}],
       ["filtered.test.default", 1391832864, {"record"=>"test.default,1391832864,{\"key9\"=>\"val9\"}"}]]
    )
  end

  it 'should be rewritten to the specified tag' do
    emits = run_driver(:prefix => 'any_prefix') do |d|
      (0...10).each do |i|
        d.emit({"key#{i}" => "val#{i}"}, time + i)
      end
    end

    expect(emits).to eq(
      [["any_prefix.test.default", 1391832855, {"record"=>"test.default,1391832855,{\"key0\"=>\"val0\"}"}],
       ["any_prefix.test.default", 1391832856, {"record"=>"test.default,1391832856,{\"key1\"=>\"val1\"}"}],
       ["any_prefix.test.default", 1391832857, {"record"=>"test.default,1391832857,{\"key2\"=>\"val2\"}"}],
       ["any_prefix.test.default", 1391832858, {"record"=>"test.default,1391832858,{\"key3\"=>\"val3\"}"}],
       ["any_prefix.test.default", 1391832859, {"record"=>"test.default,1391832859,{\"key4\"=>\"val4\"}"}],
       ["any_prefix.test.default", 1391832860, {"record"=>"test.default,1391832860,{\"key5\"=>\"val5\"}"}],
       ["any_prefix.test.default", 1391832861, {"record"=>"test.default,1391832861,{\"key6\"=>\"val6\"}"}],
       ["any_prefix.test.default", 1391832862, {"record"=>"test.default,1391832862,{\"key7\"=>\"val7\"}"}],
       ["any_prefix.test.default", 1391832863, {"record"=>"test.default,1391832863,{\"key8\"=>\"val8\"}"}],
       ["any_prefix.test.default", 1391832864, {"record"=>"test.default,1391832864,{\"key9\"=>\"val9\"}"}]]
    )
  end

  it 'should be filtered to multiple records' do
    filter = <<-EOS
      proc {|tag, time, record|
        [
          {'record1' => [tag, time, record].join(',')},
          {'record2' => [tag, time, record].reverse.join(',')},
        ]
      }
    EOS

    emits = run_driver(:filter => filter) do |d|
      (0...10).each do |i|
        d.emit({"key#{i}" => "val#{i}"}, time + i)
      end
    end

    expect(emits).to eq(
      [["filtered.test.default", 1391832855, {"record1"=>"test.default,1391832855,{\"key0\"=>\"val0\"}"}],
       ["filtered.test.default", 1391832855, {"record2"=>"{\"key0\"=>\"val0\"},1391832855,test.default"}],
       ["filtered.test.default", 1391832856, {"record1"=>"test.default,1391832856,{\"key1\"=>\"val1\"}"}],
       ["filtered.test.default", 1391832856, {"record2"=>"{\"key1\"=>\"val1\"},1391832856,test.default"}],
       ["filtered.test.default", 1391832857, {"record1"=>"test.default,1391832857,{\"key2\"=>\"val2\"}"}],
       ["filtered.test.default", 1391832857, {"record2"=>"{\"key2\"=>\"val2\"},1391832857,test.default"}],
       ["filtered.test.default", 1391832858, {"record1"=>"test.default,1391832858,{\"key3\"=>\"val3\"}"}],
       ["filtered.test.default", 1391832858, {"record2"=>"{\"key3\"=>\"val3\"},1391832858,test.default"}],
       ["filtered.test.default", 1391832859, {"record1"=>"test.default,1391832859,{\"key4\"=>\"val4\"}"}],
       ["filtered.test.default", 1391832859, {"record2"=>"{\"key4\"=>\"val4\"},1391832859,test.default"}],
       ["filtered.test.default", 1391832860, {"record1"=>"test.default,1391832860,{\"key5\"=>\"val5\"}"}],
       ["filtered.test.default", 1391832860, {"record2"=>"{\"key5\"=>\"val5\"},1391832860,test.default"}],
       ["filtered.test.default", 1391832861, {"record1"=>"test.default,1391832861,{\"key6\"=>\"val6\"}"}],
       ["filtered.test.default", 1391832861, {"record2"=>"{\"key6\"=>\"val6\"},1391832861,test.default"}],
       ["filtered.test.default", 1391832862, {"record1"=>"test.default,1391832862,{\"key7\"=>\"val7\"}"}],
       ["filtered.test.default", 1391832862, {"record2"=>"{\"key7\"=>\"val7\"},1391832862,test.default"}],
       ["filtered.test.default", 1391832863, {"record1"=>"test.default,1391832863,{\"key8\"=>\"val8\"}"}],
       ["filtered.test.default", 1391832863, {"record2"=>"{\"key8\"=>\"val8\"},1391832863,test.default"}],
       ["filtered.test.default", 1391832864, {"record1"=>"test.default,1391832864,{\"key9\"=>\"val9\"}"}],
       ["filtered.test.default", 1391832864, {"record2"=>"{\"key9\"=>\"val9\"},1391832864,test.default"}]]
    )
  end

  describe 'when an invalid record was included' do
    it 'should skip the bad records (Filter returns a Hash)' do
      filter = <<-EOS
        proc {|tag, time, record|
          if record.has_key?('return_nil')
            nil
          else
            {'record' => [tag, time, record].join(',')}
          end
        }
      EOS

      emits = run_driver(:filter => filter) do |d|
        d.instance.log.should_receive(:warn) {|msg| expect(msg).to eq('Record must be Hash: nil (NilClass)') }

        (0...10).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
        end

        d.emit({"return_nil" => 1}, time + 10)

        (10...20).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"record"=>"test.default,1391832855,{\"key0\"=>\"val0\"}"}],
         ["filtered.test.default", 1391832856, {"record"=>"test.default,1391832856,{\"key1\"=>\"val1\"}"}],
         ["filtered.test.default", 1391832857, {"record"=>"test.default,1391832857,{\"key2\"=>\"val2\"}"}],
         ["filtered.test.default", 1391832858, {"record"=>"test.default,1391832858,{\"key3\"=>\"val3\"}"}],
         ["filtered.test.default", 1391832859, {"record"=>"test.default,1391832859,{\"key4\"=>\"val4\"}"}],
         ["filtered.test.default", 1391832860, {"record"=>"test.default,1391832860,{\"key5\"=>\"val5\"}"}],
         ["filtered.test.default", 1391832861, {"record"=>"test.default,1391832861,{\"key6\"=>\"val6\"}"}],
         ["filtered.test.default", 1391832862, {"record"=>"test.default,1391832862,{\"key7\"=>\"val7\"}"}],
         ["filtered.test.default", 1391832863, {"record"=>"test.default,1391832863,{\"key8\"=>\"val8\"}"}],
         ["filtered.test.default", 1391832864, {"record"=>"test.default,1391832864,{\"key9\"=>\"val9\"}"}],
         ["filtered.test.default", 1391832865, {"record"=>"test.default,1391832865,{\"key10\"=>\"val10\"}"}],
         ["filtered.test.default", 1391832866, {"record"=>"test.default,1391832866,{\"key11\"=>\"val11\"}"}],
         ["filtered.test.default", 1391832867, {"record"=>"test.default,1391832867,{\"key12\"=>\"val12\"}"}],
         ["filtered.test.default", 1391832868, {"record"=>"test.default,1391832868,{\"key13\"=>\"val13\"}"}],
         ["filtered.test.default", 1391832869, {"record"=>"test.default,1391832869,{\"key14\"=>\"val14\"}"}],
         ["filtered.test.default", 1391832870, {"record"=>"test.default,1391832870,{\"key15\"=>\"val15\"}"}],
         ["filtered.test.default", 1391832871, {"record"=>"test.default,1391832871,{\"key16\"=>\"val16\"}"}],
         ["filtered.test.default", 1391832872, {"record"=>"test.default,1391832872,{\"key17\"=>\"val17\"}"}],
         ["filtered.test.default", 1391832873, {"record"=>"test.default,1391832873,{\"key18\"=>\"val18\"}"}],
         ["filtered.test.default", 1391832874, {"record"=>"test.default,1391832874,{\"key19\"=>\"val19\"}"}]]
      )
    end

    it 'should skip the bad records (Filter returns an Array)' do
      filter = <<-EOS
        proc {|tag, time, record|
          if record.has_key?('return_nil')
            [nil, 1]
          else
            {'record' => [tag, time, record].join(',')}
          end
        }
      EOS

      emits = run_driver(:filter => filter) do |d|
        d.instance.log.should_receive(:warn) {|msg| expect(msg).to eq('Record must be Hash: nil (NilClass)') }
        d.instance.log.should_receive(:warn) {|msg| expect(msg).to eq('Record must be Hash: 1 (Fixnum)') }

        (0...10).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
        end

        d.emit({"return_nil" => 1}, time + 10)

        (10...20).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"record"=>"test.default,1391832855,{\"key0\"=>\"val0\"}"}],
         ["filtered.test.default", 1391832856, {"record"=>"test.default,1391832856,{\"key1\"=>\"val1\"}"}],
         ["filtered.test.default", 1391832857, {"record"=>"test.default,1391832857,{\"key2\"=>\"val2\"}"}],
         ["filtered.test.default", 1391832858, {"record"=>"test.default,1391832858,{\"key3\"=>\"val3\"}"}],
         ["filtered.test.default", 1391832859, {"record"=>"test.default,1391832859,{\"key4\"=>\"val4\"}"}],
         ["filtered.test.default", 1391832860, {"record"=>"test.default,1391832860,{\"key5\"=>\"val5\"}"}],
         ["filtered.test.default", 1391832861, {"record"=>"test.default,1391832861,{\"key6\"=>\"val6\"}"}],
         ["filtered.test.default", 1391832862, {"record"=>"test.default,1391832862,{\"key7\"=>\"val7\"}"}],
         ["filtered.test.default", 1391832863, {"record"=>"test.default,1391832863,{\"key8\"=>\"val8\"}"}],
         ["filtered.test.default", 1391832864, {"record"=>"test.default,1391832864,{\"key9\"=>\"val9\"}"}],
         ["filtered.test.default", 1391832865, {"record"=>"test.default,1391832865,{\"key10\"=>\"val10\"}"}],
         ["filtered.test.default", 1391832866, {"record"=>"test.default,1391832866,{\"key11\"=>\"val11\"}"}],
         ["filtered.test.default", 1391832867, {"record"=>"test.default,1391832867,{\"key12\"=>\"val12\"}"}],
         ["filtered.test.default", 1391832868, {"record"=>"test.default,1391832868,{\"key13\"=>\"val13\"}"}],
         ["filtered.test.default", 1391832869, {"record"=>"test.default,1391832869,{\"key14\"=>\"val14\"}"}],
         ["filtered.test.default", 1391832870, {"record"=>"test.default,1391832870,{\"key15\"=>\"val15\"}"}],
         ["filtered.test.default", 1391832871, {"record"=>"test.default,1391832871,{\"key16\"=>\"val16\"}"}],
         ["filtered.test.default", 1391832872, {"record"=>"test.default,1391832872,{\"key17\"=>\"val17\"}"}],
         ["filtered.test.default", 1391832873, {"record"=>"test.default,1391832873,{\"key18\"=>\"val18\"}"}],
         ["filtered.test.default", 1391832874, {"record"=>"test.default,1391832874,{\"key19\"=>\"val19\"}"}]]
      )
    end
  end

  describe 'when the non-Proc filter passed' do
    it 'should be filtered' do
      filter = <<-EOS
        class Filter
          def call(tag, time, records)
            @total_count ||= 0
            @total_count += records.count
            {'total_count' => @total_count}
          end
        end
        Filter.new
      EOS

      emits = run_driver(:filter => filter) do |d|
        (0...10).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"total_count"=>1}],
         ["filtered.test.default", 1391832856, {"total_count"=>2}],
         ["filtered.test.default", 1391832857, {"total_count"=>3}],
         ["filtered.test.default", 1391832858, {"total_count"=>4}],
         ["filtered.test.default", 1391832859, {"total_count"=>5}],
         ["filtered.test.default", 1391832860, {"total_count"=>6}],
         ["filtered.test.default", 1391832861, {"total_count"=>7}],
         ["filtered.test.default", 1391832862, {"total_count"=>8}],
         ["filtered.test.default", 1391832863, {"total_count"=>9}],
         ["filtered.test.default", 1391832864, {"total_count"=>10}]]
      )
    end
  end
end
