class Fluent::BufferedFilterOutput < Fluent::BufferedOutput
  Fluent::Plugin.register_output('buffered_filter', self)

  unless method_defined?(:log)
    define_method('log') { $log }
  end

  config_param :filter_path, :type => :string
  config_param :prefix,      :type => :string, :default => 'filtered'

  def configure(conf)
    super

    unless File.exist?(@filter_path)
      raise Fluent::ConfigError, "No such file: #{@filter_path}"
    end

    begin
      @filter = Object.new.instance_eval(File.read(@filter_path), @filter_path)
    rescue => e
      raise Fluent::ConfigError, "Invalid filter: #{@filter_path}: #{e}"
    end

    unless @filter.respond_to?(:call)
      raise Fluent::ConfigError, "`call` method not implemented in filter: #{@filter_path}"
    end
  end

  def format(tag, time, record)
    [tag, time, record].to_msgpack
  end

  def write(chunk)
    chunk.msgpack_each do |tag, time, record|
      records = @filter.call(tag, time, record)
      records = [records] unless records.kind_of?(Array)
      emit_records(tag, time, records)
    end
  end

  private

  def emit_records(tag, time, records)
    records.each do |record|
      if record.kind_of?(Hash)
        Fluent::Engine.emit("#{@prefix}.#{tag}", time, record)
      else
        log.warn("Record must be Hash: #{record.inspect} (#{record.class})")
      end
    end
  end
end
