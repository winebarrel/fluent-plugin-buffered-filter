# coding: utf-8
Gem::Specification.new do |spec|
  spec.name          = 'fluent-plugin-buffered-filter'
  spec.version       = '0.1.0'
  spec.authors       = ['Genki Sugawara']
  spec.email         = ['sgwr_dts@yahoo.co.jp']
  spec.description   = %q{Versatile filtering plugin}
  spec.summary       = %q{Versatile filtering plugin}
  spec.homepage      = 'https://bitbucket.org/winebarrel/fluent-plugin-buffered-filter'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'fluentd'
  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '>= 2.11.0'
end
